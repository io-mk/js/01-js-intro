set terminal pngcairo transparent enhanced fontscale 1.0 size 640,480
set encoding utf8
set style data histograms
set style histogram rowstacked
set boxwidth 0.5
set style fill solid 1.0 border -1
set xtics nomirror rotate by 36 right out
set yrange [0:10]
set ytics nomirror out
set ylabel "I/O latency in microseconds" offset 1,0
set datafile separator " "
set rmargin at screen 0.6
set linetype 1 lc rgb 'red'
set style fill solid 0.5
set arrow from -0.5, 4.9 to 4.3, 4.9 nohead
set label 1 at 4.5, 5
set label 1 "Total synchronous cost\nlower than asynchronous\nOS cost alone!\n(Avoid overhead for\ninterrupt handling,\nscheduling, context\nswitching.)"
plot '2-YMH12.dat' using 2 title "Operating System", '' using 3:xticlabels(1) t "Hardware Device", '' using :($2+$3):(sprintf('%.1f', $2+$3)) notitle w labels offset 0,0.4
